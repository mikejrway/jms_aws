package com.ltree;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class AwsMessageSender {

    public SQSConnection getConnection() throws JMSException {
// Create a new connection factory with all defaults (credentials and region) set automatically
        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.defaultClient()
        );

// Create the connection.
        return connectionFactory.createConnection();
    }

    public static void main(String args[]) throws JMSException {
        QueueConfig config = QueueConfig.parseConfig("TextMessageSender", args);

       // ExampleCommon.setupLogging();

        // Create the connection factory based on the config
        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.standard()
                        .withRegion(config.getRegion().getName())
                        .withCredentials(config.getCredentialsProvider())
        );

        // Create the connection -- Specific to AWS SQS
        // TODO 1 create connection from the connection facctory
        SQSConnection connection = connectionFactory.createConnection();

        // Create the queue if needed -- Specific to AWS SQS
        MessageUtils.ensureQueueExists(connection, config.getQueueName());

        // Create the session JMS -- from here on in
        // TODO 2 Create a session from the connection
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // TODO 3 Create a message producer from the session
        MessageProducer producer = session.createProducer( session.createQueue( config.getQueueName() ) );

        // TODO 4 Create a text message
        TextMessage message = session.createTextMessage("My Message text");

        // TODO 5 Use the producer to send the message
        producer.send(message);

        // Close the connection. This closes the session automatically
        connection.close();
        System.out.println( "Connection closed" );
    }

}
