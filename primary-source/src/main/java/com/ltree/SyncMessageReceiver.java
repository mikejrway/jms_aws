package com.ltree;


import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

import javax.jms.*;
import java.util.concurrent.TimeUnit;

public class SyncMessageReceiver {
    public static void main(String args[])  {
        try {
            new SyncMessageReceiver().sendMessage( args );
        } catch (JMSException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private void sendMessage( String args[] ) throws JMSException{
        QueueConfig config = QueueConfig.parseConfig("SyncMessageReceiver", args);

        MessageUtils.setupLogging();

        // Create the connection factory based on the config  --- SQS specific
        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.standard()
                        .withRegion(config.getRegion().getName())
                        .withCredentials(config.getCredentialsProvider())
        );
        // TODO 1 Note that the creation of the connection and the session is identical to that for the
        // receive

        // Create the connection --- SQS specific
        SQSConnection connection = connectionFactory.createConnection();

        // Create the queue if needed --- SQS specific
        MessageUtils.ensureQueueExists(connection, config.getQueueName());

        // Create the session JMS from here
        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

        // TODO 2 Create a message consumer from the session
        MessageConsumer consumer = session.createConsumer( session.createQueue( config.getQueueName() ) );
        // TODO 3 Start the connection
        connection.start();


        while( true ) {
            System.out.println( "Waiting for messages");
            // Wait 1 minute for a message
            // TODO 4 Call the receive message on the consumer with a timeout of 1 minute
            Message message = consumer.receive(TimeUnit.MINUTES.toMillis(1));
            if( message == null ) {
                System.out.println( "Shutting down after 1 minute of silence" );
                break;
            }
            // TODO 5 extract the text from the message and print it.
            TextMessage txtMessage = ( TextMessage ) message;
            System.out.println( "\t" + txtMessage.getText() );

            message.acknowledge();
            System.out.println( "Acknowledged message " + message.getJMSMessageID() );
        }

        // Close the connection. This closes the session automatically
        connection.close();
        System.out.println( "Connection closed" );
    }




}