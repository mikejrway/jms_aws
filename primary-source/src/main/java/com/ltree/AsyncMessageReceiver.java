package com.ltree;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

import javax.jms.*;
import java.util.concurrent.TimeUnit;

public class AsyncMessageReceiver {
    public static void main(String args[])  {
        try {
            new AsyncMessageReceiver().ReceiveMessage( args );
        } catch (JMSException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private void ReceiveMessage(String args[] ) throws JMSException{
        QueueConfig config = QueueConfig.parseConfig("SyncMessageReceiver", args);

        System.out.println( "Starting Receive" );
        MessageUtils.setupLogging();

        // Create the connection factory based on the config  --- SQS specific
        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.standard()
                        .withRegion(config.getRegion().getName())
                        .withCredentials(config.getCredentialsProvider())
        );

        // Create the connection --- SQS specific
        SQSConnection connection = connectionFactory.createConnection();

        // Create the queue if needed --- SQS specific
        MessageUtils.ensureQueueExists(connection, config.getQueueName());

        // Create the session JMS from here
        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        MessageConsumer consumer = session.createConsumer( session.createQueue( config.getQueueName() ) );
        
        // Async message RX
        MyListener msgListener = new MyListener();
        // Instantiate and set the message listener for the consumer.
        consumer.setMessageListener(msgListener);

        // Start receiving incoming messages.
        connection.start();

        // Wait for a minute of silence
        try {
            msgListener.waitForOneMinuteOfSilence();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Close the connection. This closes the session automatically
        connection.close();
        System.out.println( "Connection closed" );
    }


    class MyListener implements MessageListener {
        private volatile long timeOfLastMessage = System.nanoTime();

        public void waitForOneMinuteOfSilence() throws InterruptedException {
            for(;;) {
                long timeSinceLastMessage = System.nanoTime() - timeOfLastMessage;
                long remainingTillOneMinuteOfSilence =
                        TimeUnit.MINUTES.toNanos(1) - timeSinceLastMessage;
                if( remainingTillOneMinuteOfSilence < 0 ) {
                    break;
                }
                TimeUnit.NANOSECONDS.sleep(remainingTillOneMinuteOfSilence);
            }
        }
        @Override
        public void onMessage(Message message) {
            try {
                // Cast the received message as TextMessage and print the text to screen.
                System.out.println("Received: " + ((TextMessage) message).getText());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
}